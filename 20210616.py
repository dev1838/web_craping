#imports here
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
from datetime import datetime
import pandas as pd
import re
import sqlite3




chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)


#specify the path to chromedriver.exe (download and save on your computer)
driver = webdriver.Chrome('C:/Users/LAU/Desktop/fb/chromedriver.exe', chrome_options=chrome_options)

#open the webpage
driver.get("http://www.facebook.com")

#target username
username = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pass']")))

#enter username and password
username.clear()
username.send_keys("fortestgithub@gmail.com")
password.clear()
password.send_keys("Github123")

#target the login button and click it
button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

#We are logged in!


time.sleep(5)
links = [] 

print('Please input keywords:')
keyword = str(input())
search_link = 'https://www.facebook.com/search/pages?q=' + keyword + '&filters=eyJmaWx0ZXJfcGFnZXNfbG9jYXRpb246MCI6IntcIm5hbWVcIjpcImZpbHRlcl9wYWdlc19sb2NhdGlvblwiLFwiYXJnc1wiOlwiMTEzMzE3NjA1MzQ1NzUxXCJ9In0%3D'
driver.get(search_link)
time.sleep(5)
    
    #scroll down
    #increase the range to sroll more
for j in range(0,10):
    try:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(8)
        print("Reading Page", j)
    except:
        break

    #target all the link elements on the page
anchors = driver.find_elements_by_tag_name('a')
anchors = [a.get_attribute('href') for a in anchors]
anchors = [a for a in anchors if str(a).startswith("https://www.facebook.com/")]

for a in anchors:
    if '?__tn__=%3' in a :
        new_a = a[:-12]
        links.append(new_a)



print("Starting Part 2: ")
con = sqlite3.connect('result.db')
cur = con.cursor()

#cur.execute('''CREATE TABLE Information
#                (Company text, Facebook text, Phone text, Messenger text, Description text, Email text, Website text, LatestPost text, ScrapingTime time, PageCreatedDate time)''')


for link in links:
    try:
        driver.get(link)
        time.sleep(5)

        temp_lst = {'Company': '?', 'Facebook': '?', 'Phone': '?', 'Messenger': '?', 'Description': '?', 'Email': '?', 'Website': '?', 'LatestPost':'?', 'ScrapingTime':datetime.now().strftime("%Y-%m-%d"), 'PageCreatedDate':'?'}
        temp_lst['Facebook'] = link

        header = driver.find_elements_by_css_selector("div[class='datstx6m cbu4d94t j83agx80']")
        header = header[0]
        child = header.find_elements_by_css_selector("*")
        all_inform = []
        for i in child:
            if i.text not in all_inform:
                all_inform.append(i.text)


        phone_pattern1 = '[0-9]{4}\s[0-9]{4}'
        phone_pattern2 = '[0-9]{8}'
        for each in all_inform:
            if re.fullmatch(phone_pattern1, each) != None or re.fullmatch(phone_pattern2, each) != None:
                temp_lst['Phone'] = each
            if 'www' in each or 'http' in each:
                temp_lst['Website'] = each
            if '@' in each:
                temp_lst['Email'] = each

        date_created_list = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v lrazzd5p oo9gr5id']")
        created_date = '?'
        for i in date_created_list:
            if "Page created" in i.text:
                created_date = i.text[15:]
        temp_lst['PageCreatedDate'] = created_date
        
        
        post = driver.find_elements_by_css_selector("a[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw']")
        post_time = []
        for i in post:
            post_time.extend(i.find_elements_by_tag_name("span"))
        post_time = [t.text for t in post_time]
        temp_lst['LatestPost'] = post_time[0]


        elements = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 embtmqzv fe6kdd0r mau55g9w c8b282yb hrzyx87i m6dqt4wy h7mekvxk hnhda86s oo9gr5id hzawbc8m'")
        comp_name = elements[0].find_elements_by_tag_name("span")[0].text
        if(len(comp_name) > 0):
            temp_lst['Company'] = comp_name
        
        pattern3 = 'https://www.facebook.com/messages/t/'
        pt = 'https://www.facebook.com/'
        temp_lst['Messenger'] = pattern3+link[len(pt):]


        about_link = link + '/about/?ref=page_internal'
        driver.get(about_link)
        time.sleep(5)
        
        if('See more' in (a.text for a in driver.find_elements_by_css_selector("div[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl oo9gr5id gpro0wi8 lrazzd5p']"))):
            button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl oo9gr5id gpro0wi8 lrazzd5p']"))).click()
        
        find_div_tag = driver.find_elements_by_css_selector("div[class='kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql']")
        find_div_tag.extend(driver.find_elements_by_css_selector("div[class='o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql']"))
        for i in find_div_tag:
            temp = i.find_elements_by_tag_name("div")
            if len(temp)>0:
                temp_lst['Description'] += temp[0].text + '\n'


        cur.execute("""SELECT Count(*) FROM Information
                        WHERE Facebook = ?""", (temp_lst['Facebook'],))
        data = cur.fetchone()[0]
        if data == 0:
            sqlite_insert_with_param = """INSERT INTO Information
                              (Company, Facebook, Phone, Messenger, Description, Email, Website, LatestPost, ScrapingTime, PageCreatedDate) 
                              SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                              WHERE NOT EXISTS
                                (SELECT Facebook FROM Information
                                    WHERE Facebook = ? and LatestPost = ?);"""

            data_tuple = (temp_lst['Company'], temp_lst['Facebook'], temp_lst['Phone'], temp_lst['Messenger'], temp_lst['Description'], temp_lst['Email'], temp_lst['Website'], temp_lst['LatestPost'], temp_lst['ScrapingTime'], temp_lst['PageCreatedDate'], temp_lst['Facebook'], temp_lst['LatestPost'])
            cur.execute(sqlite_insert_with_param, data_tuple)
            con.commit()
            print("Record ", temp_lst['Company'], temp_lst['Phone'])
        else:
            print("Data Exist :", temp_lst['Company'])
            
    except:
       continue

con.commit()
con.close()
