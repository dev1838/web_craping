#imports here
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
import pandas as pd

chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)


#specify the path to chromedriver.exe (download and save on your computer)
driver = webdriver.Chrome('C:/Users/LAU/Desktop/fb/chromedriver.exe', chrome_options=chrome_options)

#open the webpage
driver.get("http://www.facebook.com")

#target username
username = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pass']")))

#enter username and password
username.clear()
username.send_keys("fortestgithub@gmail.com")
password.clear()
password.send_keys("Github123")

#target the login button and click it
button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

#We are logged in!


time.sleep(5)
links = [] 

print('Please input keywords:')
keyword = str(input())
search_link = 'https://www.facebook.com/search/pages?q=' + keyword + '&filters=eyJmaWx0ZXJfcGFnZXNfbG9jYXRpb246MCI6IntcIm5hbWVcIjpcImZpbHRlcl9wYWdlc19sb2NhdGlvblwiLFwiYXJnc1wiOlwiMTEzMzE3NjA1MzQ1NzUxXCJ9In0%3D'
driver.get(search_link)
time.sleep(5)
    
    #scroll down
    #increase the range to sroll more
for j in range(0,10):
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(8)
    print(j)

    #target all the link elements on the page
anchors = driver.find_elements_by_tag_name('a')
anchors = [a.get_attribute('href') for a in anchors]
anchors = [a for a in anchors if str(a).startswith("https://www.facebook.com/")]

for a in anchors:
    if '?__tn__=%3' in a :
        new_a = a[:-12]
        links.append(new_a)

df = pd.DataFrame(links)
df.to_excel('list_fb.xlsx')
