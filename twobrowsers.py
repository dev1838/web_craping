#imports here
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
from datetime import datetime
import pandas as pd
import re
import sqlite3


def findPost(driver):
    all_post = driver.find_elements_by_css_selector("div[class='k4urcfbm dp1hu0rb d2edcug0 cbu4d94t j83agx80 bp9cbjyn']")
    #print(len(all_post))

    post_type = all_post[0].find_elements_by_css_selector("div[role='feed']")
    #print(len(post_type))

    if len(post_type) > 0:
        latest = post_type[-1].find_elements_by_css_selector("a[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw']")
    else:
        latest = all_post[0].find_elements_by_css_selector("a[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw']")    
    #print(latest[0].text)
    init_text = latest[0].text.split('\n')
    #print(init_text)
    tmp = latest[0].find_elements_by_css_selector(".b6zbclly.myohyog2.l9j0dhe7.aenfhxwr.l94mrbxd.ihxqhq3m.nc684nl6.t5a262vz.sdhka5h4[style='position: absolute; top: 3em;']")
    tmp = [i.text for i in tmp]
    #print(tmp)
    my_text = ''
    first_cur = 0
    second_cur = 0
    new_init_text = []
    for text in init_text:
        for cha in text:
            new_init_text.append(cha)
    new_tmp = []
    for text in tmp:
        for cha in text:
            new_tmp.append(cha)
    init_text = new_init_text
    tmp = new_tmp
    while(first_cur < len(init_text) and second_cur < len(tmp)):
        if init_text[first_cur] == tmp[second_cur]:
            first_cur+=1
            second_cur+=1
        else:
            my_text+=init_text[first_cur]
            first_cur+=1
    return my_text

def getPageData2(link, driver, ScrollNumber, keyword):
    phone_pattern = '[0-9]{4}\s[0-9]{4}'
    inf_lst = {'Company': '?', 'Facebook': '?', 'Phone': '?', 'Messenger': '?', 'Description': '', 'Email': '?', 'Website': '?', 'NumFollowers': 0, 'LatestPost':'?', 'ScrapingTime':datetime.now().strftime("%Y-%m-%d"), 'PageCreatedDate':'?', 'BusinessClassification':'?'}
    inf_lst['Facebook'] = link
    try:
        elements = driver.find_elements_by_css_selector("h1[class='gmql0nx0 l94mrbxd p1ri9a11 lzcic4wl'")
        inf_lst['Company'] = elements[0].text
    except:
        inf_lst['Company'] = 'error'
    pattern3 = 'https://www.facebook.com/messages/t/'
    pt = 'https://www.facebook.com/'
    inf_lst['Messenger'] = pattern3+link[len(pt):]

    try:
        intro = driver.find_elements_by_css_selector("div[class='rq0escxv l9j0dhe7 du4w35lb hybvsw6c io0zqebd m5lcvass fbipl8qg nwvqtn77 k4urcfbm ni8dbmo4 stjgntxs sbcfpzgs']")
        intro_text = intro[0].text
        intro_text = intro_text.split('\n')
        #print(intro_text)
        for each_text in intro_text:
            if re.fullmatch(phone_pattern, each_text):
                inf_lst['Phone'] = each_text
            if '@' in each_text:
                inf_lst['Email'] = each_text
            if 'http' in each_text or '.com' in each_text:
                inf_lst['Website'] = each_text
            if 'followers' in each_text:
                array = re.findall(r'[0-9]+', each_text)
                if len(array)>0:
                    inf_lst['NumFollowers'] = int(array[0])

        for each_text in intro_text:
            if each_text == inf_lst['Phone'] or each_text == inf_lst['Email'] or each_text == inf_lst['Website'] or each_text == inf_lst['NumFollowers']:
                continue
            if 'Likes' not in each_text:
                inf_lst['Description'] += each_text + '\n'
    except:
        inf_lst['Phone'] = 'error'
        inf_lst['Email'] = 'error'
        inf_lst['Website'] = 'error'
        inf_lst['NumFollowers'] = 'error'
        inf_lst['Description'] = 'error'
    try:    
        latest = driver.find_elements_by_css_selector("a[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw']")[0]
        #print(latest[0].text)
        init_text = latest[0].text.split('\n')
        #print(init_text)
        tmp = latest[0].find_elements_by_css_selector(".b6zbclly.myohyog2.l9j0dhe7.aenfhxwr.l94mrbxd.ihxqhq3m.nc684nl6.t5a262vz.sdhka5h4[style='position: absolute; top: 3em;']")
        tmp = [i.text for i in tmp]
        #print(tmp)
        my_text = ''
        first_cur = 0
        second_cur = 0
        new_init_text = []
        for text in init_text:
            for cha in text:
                new_init_text.append(cha)
        new_tmp = []
        for text in tmp:
            for cha in text:
                new_tmp.append(cha)
        init_text = new_init_text
        tmp = new_tmp
        while(first_cur < len(init_text) and second_cur < len(tmp)):
            if init_text[first_cur] == tmp[second_cur]:
                first_cur+=1
                second_cur+=1
            else:
                my_text+=init_text[first_cur]
                first_cur+=1
        inf_lst['LatestPost'] = my_text
    except:
        inf_lst['LatestPost'] = 'error'
    try:
        new_link = link + '/about_profile_transparency'
        driver.get(new_link)
        time.sleep(2)
        find_block = driver.find_elements_by_css_selector("div[class='dati1w0a tu1s4ah4 f7vcsfb0 discj3wi']")[0]
        sjgh65i0_class = find_block.find_elements_by_css_selector("div[class='sjgh65i0']")
        for elem in sjgh65i0_class:
            if 'Page creation date' in elem.text:
                inf_lst['PageCreatedDate'] = elem.find_elements_by_css_selector("div[class='ii04i59q a3bd9o3v jq4qci2q oo9gr5id tvmbv18p']")[0].text
        inf_lst['BusinessClassification'] = keyword
    except:
        inf_lst['BusinessClassification'] = 'error'
    Insert_to_db(inf_lst, ScrollNumber, keyword)

def getPageData(link, driver, ScrollNumber, keyword):
    inf_lst = {'Company': '?', 'Facebook': '?', 'Phone': '?', 'Messenger': '?', 'Description': '', 'Email': '?', 'Website': '?', 'NumFollowers': 0, 'LatestPost':'?', 'ScrapingTime':datetime.now().strftime("%Y-%m-%d"), 'PageCreatedDate':'?', 'BusinessClassification':'?'}
    inf_lst['Facebook'] = link
    driver.get(link)
    time.sleep(3)
        
    try:
        elements = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 embtmqzv fe6kdd0r mau55g9w c8b282yb hrzyx87i m6dqt4wy h7mekvxk hnhda86s oo9gr5id hzawbc8m'")
        comp_name = elements[0].find_elements_by_tag_name("span")[0].text
        if(len(comp_name) > 0):
            inf_lst['Company'] = comp_name
    except:
        getPageData2(link, driver, ScrollNumber, keyword)
        return
    
    pattern3 = 'https://www.facebook.com/messages/t/'
    pt = 'https://www.facebook.com/'
    inf_lst['Messenger'] = pattern3+link[len(pt):]
    
    try:
        #find about position
        list_sjgh65i0 = driver.find_elements_by_css_selector("div[class='sjgh65i0']")
        about_index = 0
        break_point = False
        for index in range(len(list_sjgh65i0)):
            if(break_point):
                break
            list_about_class = list_sjgh65i0[index].find_elements_by_css_selector("span[class='a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 r8blr3vg']")
            for each in list_about_class:
                if each.text == 'About':
                    about_index = index
                    break_point = True
                    break
        about_block = list_sjgh65i0[about_index]
        see_more_buttons = about_block.find_elements_by_css_selector("div[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl oo9gr5id gpro0wi8 lrazzd5p']")
        for each in see_more_buttons:
            if 'See more' in each.text:
                each.click()
        #map_list = about_block.find_elements_by_css_selector("div[class='l9j0dhe7 dhix69tm wkznzc2l p5pk11vy o9dbymsk j83agx80 kzx2olss aot14ch1 p86d2i9g beltcj47 o9q50ak6 j1vyfwqu jm7nytfl rg76wyzq tdjehn4e']")
        phone_pattern = '[0-9]{4}\s[0-9]{4}'
        list_j83agx80 = list_sjgh65i0[about_index].find_elements_by_css_selector("div[class='j83agx80']")
        like_follow = list_sjgh65i0[about_index].find_elements_by_css_selector("div[class='taijpn5t cbu4d94t j83agx80']")
        for num in like_follow:
            if 'follow' in num.text:
                array = re.findall(r'[0-9]+', num.text)
                inf_lst['NumFollowers'] = int(array[0])
                break
        all_children = []
        list_text = []
        for each in list_j83agx80:
            classify = each.find_elements_by_css_selector("a[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl oo9gr5id gpro0wi8 lrazzd5p']")
            if(len(classify) > 0):
                bus_class = ''
                for typ in classify:
                    bus_class+= typ.text + ' . '
                inf_lst['BusinessClassification'] = bus_class[:-2]
                continue
            list_text.extend(each.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh oo9gr5id hzawbc8m']"))
        list_text = [a.text for a in list_text]
        #if len(map_list) > 0:
        #    list_text = list_text[1:]
        for text in list_text:
            if re.fullmatch(phone_pattern, text) != None:
                inf_lst['Phone'] = text
            if '@' in text:
                inf_lst['Email'] = text
            if 'www' in text or 'http' in text:
                inf_lst['Website'] = text   
        for text in list_text:
            if text == inf_lst['Phone'] or text == inf_lst['Email'] or text == inf_lst['Website'] or text == inf_lst['BusinessClassification']:
                continue
            if 'Open now' not in text and 'Close now' not in text and 'Price Range' not in text:
                inf_lst['Description'] += text + '\n'
        if inf_lst['Description'] == '':
            inf_lst['Description'] = '?'

    except:
        inf_lst['Description'] = 'error'
        inf_lst['Phone'] = 'error'
        inf_lst['Website'] = 'error'
        inf_lst['Email'] = 'error'
        inf_lst['BusinessClassification'] = 'error'
        
    try:
        inf_lst['LatestPost'] = findPost(driver)
    except:
        inf_lst['LatestPost'] = 'No post'
    
    try:
        date_created_list = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v lrazzd5p oo9gr5id']")
        created_date = '?'
        for i in date_created_list:
            if "Page created" in i.text:
                created_date = i.text[15:]
        inf_lst['PageCreatedDate'] = created_date
    except:
        inf_lst['PageCreatedDate'] = 'error'
    Insert_to_db(inf_lst, ScrollNumber, keyword)

def Insert_to_db(inf_lst, ScrollNumber, keyword):
    con = sqlite3.connect('facebook.db')
    cur = con.cursor()
    cur.execute("""SELECT Count(*) FROM _Master
                    WHERE Facebook = ?""", (inf_lst['Facebook'],))
    data = cur.fetchone()[0]
    if data == 0:
        sqlite_insert_with_param = """INSERT INTO _Master
                        (Company, Facebook, Phone, Messenger, Description, Email, Website, NumFollowers, LatestPost, ScrapingTime, PageCreatedDate, BusinessClassification, ScrollNumber, Keyword) 
                          SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                          WHERE NOT EXISTS
                            (SELECT Facebook FROM _Master
                                WHERE Facebook = ?);"""
        data_tuple = (inf_lst['Company'], inf_lst['Facebook'], inf_lst['Phone'], inf_lst['Messenger'], inf_lst['Description'], inf_lst['Email'], inf_lst['Website'], inf_lst['NumFollowers'], inf_lst['LatestPost'], inf_lst['ScrapingTime'], inf_lst['PageCreatedDate'], inf_lst['BusinessClassification'],ScrollNumber, keyword, inf_lst['Facebook'])
        cur.execute(sqlite_insert_with_param, data_tuple)
        con.commit()
        print("Record ", inf_lst['Company'], inf_lst['Phone'])
        try:
            cur.execute('''CREATE TABLE ''' + keyword +
                        '''(Company text, Facebook text, Phone text, Messenger text, Description text, Email text, Website text, NumFollowers number,LatestPost text, ScrapingTime time, PageCreatedDate time, BusinessClassification text, ScrollNumber number, Keyword text)''')
        except:
            print('')
        query = """INSERT INTO """ + keyword + """(Company, Facebook, Phone, Messenger, Description, Email, Website, NumFollowers, LatestPost, ScrapingTime, PageCreatedDate, BusinessClassification, ScrollNumber, Keyword) 
                          SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?;"""
        data_tuple = (inf_lst['Company'], inf_lst['Facebook'], inf_lst['Phone'], inf_lst['Messenger'], inf_lst['Description'], inf_lst['Email'], inf_lst['Website'], inf_lst['NumFollowers'], inf_lst['LatestPost'], inf_lst['ScrapingTime'], inf_lst['PageCreatedDate'], inf_lst['BusinessClassification'],ScrollNumber, keyword)
        cur.execute(query, data_tuple)
    else:
        cur.execute("""SELECT Count(*) FROM _Master
                    WHERE Facebook = ? AND LatestPost = ?""", (inf_lst['Facebook'], inf_lst['LatestPost'],))
        data = cur.fetchone()[0]
        if data == 0:
            cur.execute("""UPDATE _Master
                    SET LatestPost = ?, ScrapingTime = ?
                    WHERE Facebook = ?""", (inf_lst['LatestPost'], datetime.now().strftime("%Y-%m-%d"), inf_lst['Facebook']))
            print("Update ", inf_lst['Company'], inf_lst['Phone'])
            cur.execute("""UPDATE """ + keyword +
                    """SET LatestPost = ?, ScrapingTime = ?
                    WHERE Facebook = ?""", (inf_lst['LatestPost'], datetime.now().strftime("%Y-%m-%d"), inf_lst['Facebook']))            
        else:
            print(inf_lst['Company'], " already exists")

    con.commit()
    con.close()

chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)

#specify the path to chromedriver.exe (download and save on your computer)
driver = webdriver.Chrome('C:/Users/LAU/Desktop/fb/chromedriver.exe', chrome_options=chrome_options)

#open the webpage
driver.get("http://www.facebook.com")

#target username
username = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pass']")))

#enter username and password
username.clear()
username.send_keys("fortestgithub@gmail.com")
password.clear()
password.send_keys("Github123")

#target the login button and click it
button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

#print('Please input keywords:')
#keyword = str(input())

driver2 = webdriver.Chrome('C:/Users/LAU/Desktop/fb/chromedriver.exe', chrome_options=chrome_options)
driver2.get("http://www.facebook.com")
username2 = WebDriverWait(driver2, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
password2 = WebDriverWait(driver2, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pass']")))
username2.clear()
username2.send_keys("fortestgithub@gmail.com")
password2.clear()
password2.send_keys("Github123")
button = WebDriverWait(driver2, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()
time.sleep(3)

#We are logged in!
con = sqlite3.connect('facebook.db')
cur = con.cursor()

try:
    cur.execute('''CREATE TABLE _Master
                (Company text, Facebook text, Phone text, Messenger text, Description text, Email text, Website text, NumFollowers number,LatestPost text, ScrapingTime time, PageCreatedDate time, BusinessClassification text, ScrollNumber number, Keyword text)''')
except:
    print("Continue")

con.commit()
con.close()

key_list = ['Advertising', 'marketing', 'Agriculture', 'Arts', 'entertainment','Beauty', 'cosmetic', 'personal care',
'Commercial', 'industrial', 'Education', 'Finance', 'Food', 'drink', 'Hotel', 'Legal', 'Local service',
'Media', 'news company', 'Medical', 'health', 'Non-governmental organisation', 'Non-profit organisation',
'Property', 'Public service', 'government service', 'Science', 'technology', 'engineering', 'Shopping', 'retail',
'Sport', 'recreation', 'Travel', 'transport', 'Vehicle', 'aircraft', 'boat']

for keyword in key_list:
    search_link = 'https://www.facebook.com/search/pages?q=' + keyword + '&filters=eyJmaWx0ZXJfcGFnZXNfbG9jYXRpb246MCI6IntcIm5hbWVcIjpcImZpbHRlcl9wYWdlc19sb2NhdGlvblwiLFwiYXJnc1wiOlwiMTEzMzE3NjA1MzQ1NzUxXCJ9In0%3D'
    driver.get(search_link)
    time.sleep(3)

    con = sqlite3.connect('facebook.db')
    cur = con.cursor()
    cur.execute("""SELECT ScrollNumber FROM _Master
                    WHERE Keyword = ?
                    ORDER BY ScrollNumber DESC""", (keyword,))
    if cur.fetchone() != None:
        pos = cur.fetchone()[0] + 1
    else:
        pos = 0
    cur.execute("""SELECT Count(Facebook) FROM _Master
                    WHERE Keyword = ?""", (keyword,))
    len_pre = cur.fetchone()[0]
    all_links = []
    block_search = driver.find_elements_by_css_selector("div[class='rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t g5gj957u d2edcug0 hpfvmrgz rj1gh0hx buofh1pr dp1hu0rb']")
    block_search = block_search[0]
    #scroll down
    #increase the range to sroll more
    scroll_range = 2
    break_cond = 0
    for j in range(0, pos):
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)
    for j in range(pos,pos + scroll_range):
        try:
            links = []
            #print("Scrolling down: ", j)
            elements = block_search.find_elements_by_tag_name('a')
            for index in range(len(all_links)*2+len_pre*2, len(elements)):
                if index % 2 == 1:
                    links.append(elements[index].get_attribute('href'))
            print("Page",j, "of",keyword, ":", len(links), "links recorded")
            if len(links) == 0:
                break_cond+=1
                if break_cond == 3:
                    break
            else:
                break_cond = 0
            for link in links:
                getPageData(link, driver2, j, keyword)
            all_links.extend(links)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)
        except:
            break
